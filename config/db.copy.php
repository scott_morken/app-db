<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/3/15
 * Time: 6:36 AM
 */
 return array(
     'default' => 'db',
        'connections' => array(
            'db' => array(
                'class' => '\Smorken\Db\Connections\MysqlConnection',
                'host' => '127.0.0.1',
                'database' => 'dbname',
                'username' => 'user',
                'password' => 'secret',
            ),
        ),
 );