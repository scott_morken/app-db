## Standalone
~~~~
<?php
include 'vendors/autoload.php';
$config = array(
  'db' => array(
    'class' => '\Smorken\Db\Connections\MysqlConnection',
    'host' => '127.0.0.1',
    'database' => 'my_db',
    'username' => 'my_user',
    'password' => 'secret',
  )
);
$conn_handler = new PDOConnectionHandler($config);
$pdo_conn = $conn_handler->getConnection('db');
~~~~

## Part of simple app

Add a service line to config/app.php services array

```
'Smorken\Db\DbService'
```

~~~~
$conn_handler = $app['Smorken\Db\Contracts\ConnectionHandler'];
$pdo_conn = $conn_handler->getConnection('db');

//a model will automatically load the connection that is set in $conn_string
$protected $conn_string = 'db'; //default
~~~~