<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:07 AM
 */

namespace Smorken\Db\Contracts\Connections;


interface Connection {

    public function getConnectionString();

    public function getUsername();

    public function getPassword();

    public function getOptions();

}