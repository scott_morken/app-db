<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:01 AM
 */
namespace Smorken\Db\Contracts;


interface ConnectionHandler {

    public function createConnectionObjects($conn_config);

    public function getConnection($conn_string = null);

    public function close($conn_string = null);
}