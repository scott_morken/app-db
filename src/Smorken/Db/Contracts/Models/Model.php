<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/2/15
 * Time: 2:48 PM
 */

namespace Smorken\Db\Contracts\Models;


use Smorken\Db\Contracts\ConnectionHandler;

interface Model {

    /**
     * @param $conn_string
     * @return null
     */
    public function setConnectionString($conn_string);

    /**
     * @param $conn_handler_class
     * @return null
     */
    public function setConnectionHandlerClass($conn_handler_class);

    /**
     * @param ConnectionHandler $handler
     * @return null
     */
    public function setConnectionHandler(ConnectionHandler $handler);

    /**
     * @return \PDO
     */
    public function getPdo();

    /**
     * @return string
     */
    public function getTableName();

    /**
     * @param array $attributes
     * @return Model
     */
    public function newInstance(array $attributes = array());

    /**
     * @param array $attributes
     * @return Model|null
     */
    public function findByAttributes(array $attributes);

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id);

    /**
     * @param $sql
     * @param array $params
     * @return Model|null
     */
    public function one($sql, array $params = array());

    /**
     * @param $sql
     * @param array $params
     * @return Model[]
     */
    public function all($sql, array $params = array());

    /**
     * @param $sql
     * @param array $params
     * @param int $page
     * @param int $countper
     * @return Model[]
     */
    public function paged($sql, array $params = array(), $page = 0, $countper = 20);

    /**
     * @return bool
     */
    public function delete();

    /**
     * @param bool $validate
     * @return false|Model
     */
    public function save($validate = true);

    /**
     * @param array $attributes
     * @return null
     */
    public function fill(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return Model|false
     */
    public function update($id, array $attributes);

    /**
     * @param array $attributes
     * @return Model|false
     */
    public function insert(array $attributes);

    /**
     * @param array $attributes
     * @return bool
     */
    public function validate(array $attributes = array());

    /**
     * Return object that implements ::validate($data)
     * @return object
     */
    public function getValidator();

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param array $attributes
     * @return array
     */
    public function sanitizeAttributes(array $attributes);

    /**
     * @param string $fieldname
     * @return mixed
     */
    public function getByFieldname($fieldname);
}