<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:05 AM
 */

namespace Smorken\Db;


abstract class ConnectionHandler {

    /**
     * @var \Smorken\Db\Contracts\Connections\Connection[]
     */
    protected $conns = array();

    protected $conns_instantiated = array();

    protected $default;

    public function __construct($conn_config)
    {
        $this->createConnectionObjects($conn_config);
        $this->setDefaultConnection(array_get($conn_config, 'default', 'db'));
    }

    public function createConnectionObjects($conn_config)
    {
        foreach(array_get($conn_config, 'connections', array()) as $name => $conn_info) {
            $conn_class = $conn_info['class'];
            $this->conns[$name] = new $conn_class(
                $conn_info['host'],
                $conn_info['database'],
                array_get($conn_info, 'username', null),
                array_get($conn_info, 'password', null),
                array_get($conn_info, 'options', null)
            );
        }
    }

    public function __destruct()
    {
        $this->close();
    }

    abstract public function getConnection($conn_string = null);

    abstract public function close($conn_string = null);

    public function setDefaultConnection($conn_name)
    {
        $this->default = $conn_name;
    }

}