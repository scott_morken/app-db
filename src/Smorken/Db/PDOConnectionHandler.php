<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:07 AM
 */

namespace Smorken\Db;


class PDOConnectionHandler extends ConnectionHandler implements \Smorken\Db\Contracts\ConnectionHandler {

    public function getConnection($conn_string = null)
    {
        if ($conn_string === null) {
            $conn_string = $this->default;
        }
        if (!isset($this->conns[$conn_string])) {
            throw new \InvalidArgumentException("$conn_string is not a valid connection.");
        }
        if (!array_key_exists($conn_string, $this->conns_instantiated)) {
            /**
             * @var \Smorken\Db\Contracts\Connections\Connection $connInfo
             */
            $connInfo = $this->conns[$conn_string];
            $dbo = new \PDO($connInfo->getConnectionString(), $connInfo->getUsername(), $connInfo->getPassword(), $connInfo->getOptions());
            $dbo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $dbo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $this->conns_instantiated[$conn_string] = $dbo;
        }
        return $this->conns_instantiated[$conn_string];
    }

    public function close($conn_string = null)
    {
        $conns = ($conn_string && array_key_exists($conn_string, $this->conns_instantiated) ? [$conn_string => $this->conns_instantiated[$conn_string]] : $this->conns_instantiated);
        foreach($conns as $name => $conn) {
            if ($conn) {
                $this->conns_instantiated[$name] = null;
            }
            unset($this->conns_instantiated[$name]);
        }
    }
}