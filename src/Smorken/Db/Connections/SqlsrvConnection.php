<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/24/15
 * Time: 8:55 AM
 */

namespace Smorken\Db\Connections;


use Smorken\Db\Contracts\Connections\Connection as IConnection ;

class SqlsrvConnection extends Connection implements IConnection {

    protected $type = 'sqlsrv';

    public function getConnectionString()
    {
        return sprintf('%s:Server=%s;Database=%s', $this->type, $this->host, $this->database);
    }
}
