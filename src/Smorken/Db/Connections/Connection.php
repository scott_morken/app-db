<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/24/15
 * Time: 8:40 AM
 */

namespace Smorken\Db\Connections;


class Connection {

    protected $host;
    protected $database;
    protected $username;
    protected $password;
    protected $type = 'mysql';
    protected $options;

    public function __construct($host, $database, $username = null, $password = null, $options = array())
    {
        $this->host = $host;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
    }

    public function getConnectionString()
    {
        return sprintf('%s:host=%s;dbname=%s', $this->type, $this->host, $this->database);
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getOptions()
    {
        return $this->options;
    }
}