<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:12 AM
 */

namespace Smorken\Db\Models;


use Smorken\Application\App;
use Smorken\Db\Contracts\ConnectionHandler;
use Smorken\Db\PDOConnectionHandler;

abstract class Base {

    protected $id_l_quote = '`';

    protected $id_r_quote = '`';

    protected $schema_join = '.';

    protected $pdo;

    protected $conn_string = 'db';

    protected $conn_handler_class = 'Smorken\Db\Contracts\ConnectionHandler';

    protected $conn_handler;

    protected $attributes = array();

    protected $tablename = '';

    protected $post_fetch = array();

    protected $id_column = 'id';

    protected $fillable = array();

    protected $errors = array();

    protected $sanitize = array();

    protected $is_new = true;

    protected $auto_new = false;

    public function __construct(array $attributes = array())
    {
        if ($attributes) {
            $attributes = $this->sanitizeAttributes($attributes);
        }
        $this->attributes = $attributes;
    }

    public function setConnectionString($conn_string)
    {
        $this->conn_string = $conn_string;
    }

    public function setConnectionHandlerClass($conn_handler_class)
    {
        $this->conn_handler_class = $conn_handler_class;
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        if ($this->pdo) {
            return $this->pdo;
        }
        return $this->getConnectionFromHandler($this->conn_string);
    }

    public function getTableName()
    {
        return $this->quoteIdentifier($this->tablename);
    }

    public function quoteIdentifier($str, $fqn = false)
    {
        if (strpos($str, $this->id_l_quote) !== false) {
            $id = $str;
        }
        else {
            $expId = array_map(function($v) { return sprintf('%s%s%s', $this->id_l_quote, $v, $this->id_r_quote); }, explode($this->schema_join, $str));
            $id = implode($this->schema_join, $expId);
        }
        if (!$fqn) {
            return $id;
        }
        else {
            return $this->getTableName() . $this->schema_join . $id;
        }
    }

    public function setIsNew($is_new)
    {
        $this->is_new = $is_new;
    }

    public function getIsNew()
    {
        if ($this->auto_new) {
            return !$this->getId();
        }
        return $this->is_new;
    }

    public function find($id)
    {
        $sql = sprintf("SELECT * FROM %s WHERE %s = :idXY", $this->getTableName(), $this->quoteIdentifier($this->id_column));
        return $this->one($sql, array(':idXY' => $id));
    }

    /**
     * @param $conn_string
     * @return \PDO
     */
    protected function getConnectionFromHandler($conn_string)
    {
        $ch = $this->getConnectionHandler();
        return $ch->getConnection($conn_string);
    }

    protected function getConnectionHandler()
    {
        if (!$this->conn_handler && class_exists('\Smorken\Application\App')) {
            $app = \Smorken\Application\App::getInstance();
            $this->conn_handler = $app[$this->conn_handler_class];
        }
        if (!$this->conn_handler) {
            throw new Exception('No connection handler found.');
        }
        return $this->conn_handler;
    }

    public function setConnectionHandler(ConnectionHandler $handler)
    {
        $this->conn_handler = $handler;
    }

    public function newInstance(array $attributes = array())
    {
        return new static($attributes);
    }

    /**
     * @param array $attributes
     * @return Model|null
     */
    public function findByAttributes(array $attributes)
    {
        $sql = sprintf('SELECT * FROM %s WHERE %s',
                       $this->getTableName(),
                       implode(' AND ', $this->createSet($attributes))
        );
        return $this->one($sql, $this->parameterize($attributes, false));
    }

    public function one($sql, array $params = array())
    {
        $sth = $this->getPdo()->prepare($sql);
        $this->_execStatement($sth, $params);
        $o = $this->_yield($sth)->current();
        $sth->closeCursor();
        return $o;
    }

    public function all($sql, array $params = array(), $yield = false)
    {
        $results = array();
        $sth = $this->getPdo()->prepare($sql);
        $this->_execStatement($sth, $params);
        if ($yield) {
            return $this->_yield($sth);
        }
        foreach($this->_yield($sth) as $o) {
            $results[] = $o;
        }
        return $results;
    }

    public function paged($sql, array $params = array(), $page = 0, $countper = 20, $yield = false)
    {
        $page = (int)$page;
        $countper = (int)$countper;
        $sql .= ' LIMIT :offsetXY, :limitXY';
        $results = array();
        $sth = $this->getPdo()->prepare($sql);
        $sth->bindValue(':offsetXY', $page * $countper, \PDO::PARAM_INT);
        $sth->bindValue(':limitXY', $countper, \PDO::PARAM_INT);
        $this->_execStatement($sth, $params);
        if ($yield) {
            return $this->_yield($sth);
        }
        foreach($this->_yield($sth) as $o) {
            $results[] = $o;
        }
        return $results;
    }

    public function delete()
    {
        if ($this->getId()) {
            $sql = sprintf('DELETE FROM %s WHERE %s = :idXY', $this->getTableName(), $this->quoteIdentifier($this->id_column));
            return $this->_execWithCount($sql, array(':idXY' => $this->getId()));
        }
    }

    public function deleteWhere(array $attributes)
    {
        if (count($attributes) > 0) {
            $sql = sprintf('DELETE FROM %s WHERE %s',
                           $this->getTableName(),
                           implode(' AND ', $this->createSet($attributes))
            );
            return $this->_execWithCount($sql, $this->parameterize($attributes, false));
        }
        return 0;
    }

    public function save($validate = true)
    {
        if (!$validate || $this->validate($this->attributes)) {
            if (!$this->getIsNew()) {
                $r = $this->update($this->getId(), $this->attributes);
                if ($r) {
                    return $this->find($this->getId());
                }
                else {
                    $this->addError('update', 'Error updating model.');
                }
            }
            else {
                $r = $this->insert($this->attributes);
                if ($r) {
                    return $this->find($r);
                }
                else {
                    $this->addError('insert', 'Error inserting a new model.');
                }
            }
        }
        return false;
    }

    public function fill(array $attributes)
    {
        $attrs = $this->getFillableAttributes($attributes);
        foreach($attrs as $k => $v) {
            $this->$k = $v;
        }
    }

    public function update($id, array $attributes)
    {
        $attrs = $this->getFillableAttributes($attributes);
        $sql = sprintf('UPDATE %s SET %s WHERE %s = ?',
                       $this->getTableName(),
                       implode(', ', $this->createSet($attrs)),
                       $this->quoteIdentifier($this->id_column)
        );
        $params = $this->parameterize($attrs, false);
        $params[] = $id;
        $count = $this->_execWithCount($sql, $params);
        return $count >= 0;
    }

    public function insert(array $attributes)
    {
        $attrs = $this->getFillableAttributes($attributes);
        $sql = sprintf('INSERT INTO %s (%s) VALUES(%s)',
                       $this->getTableName(),
                       implode(', ', array_map(array($this, 'quoteIdentifier'), array_keys($attrs))),
                       implode(', ', array_fill(0, count($attrs), '?'))
        );
        return $this->_execWithLastId($sql, $this->parameterize($attrs, false));
    }

    protected function getFillableAttributes($attributes)
    {
        $attributes = $this->sanitizeAttributes($attributes);
        $fillable = array();
        foreach($this->fillable as $k) {
            if (array_key_exists($k, $attributes)) {
                $fillable[$k] = ($attributes[$k] !== '' ? $attributes[$k] : null);
            }
        }
        return $fillable;
    }

    protected function parameterize(array $attributes, $usekey = true)
    {
        $params = array();
        foreach($attributes as $k => $v) {
            if ($usekey) {
                $params[':' . $k] = $v;
            }
            else {
                $params[] = $v;
            }
        }
        return $params;
    }

    protected function createSet(array $attrs)
    {
        $set = array();
        foreach($attrs as $k => $v) {
            $set[] = sprintf("%s = ?", $this->quoteIdentifier($k));
        }
        return $set;
    }

    public function validate(array $attributes = array())
    {
        $v = $this->validators();
        if ($v === true) {
            return true;
        }
        $result = $v->validate($attributes);
        $valid = $result->isValid();
        if (!$valid) {
            foreach($result->getMessages() as $k => $msgs) {
                $this->addError($k, $msgs);
            }
        }
        return $valid;
    }

    /**
     * Return object that implements ::validate($data), ::validate must
     * return an object that implements ::isValid() and ::getMessages()
     * Set default validation for validator in this method
     * Return true to skip validation
     * @return mixed
     */
    protected function validators()
    {
        return true;
    }

    public function getValidator()
    {
        if (class_exists('\Smorken\Application\App')) {
            $app = \Smorken\Application\App::getInstance();
            return $app['validator'];
        }
        return new \Particle\Validator\Validator();
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function addError($key, $msg)
    {
        if (!array_key_exists($key, $this->errors)) {
            $this->errors[$key] = array();
        }
        if (!is_array($msg)) {
            $this->errors[$key][] = $msg;
        }
        else {
            foreach($msg as $i => $m) {
                $this->errors[$key][$i] = $m;
            }
        }
    }

    public function getId()
    {
        return array_get($this->attributes, $this->id_column, null);
    }

    protected function _execStatement(\PDOStatement &$sth, array $params = array())
    {
        if (count($params) === 0) {
            $sth->execute();
        }
        else {
            $sth->execute($params);
        }
    }

    protected function _execWithCount($sql, array $params = array())
    {
        try {
            $this->getPdo()->beginTransaction();
            $sth = $this->getPdo()->prepare($sql);
            $this->_execStatement($sth, $params);
            $affected = $sth->rowCount();
            $this->getPdo()->commit();
            return $affected;
        }
        catch (\PDOException $e)
        {
            $this->getPdo()->rollBack();
            $this->handlePDOException($e);
            return false;
        }
    }

    protected function _execWithLastId($sql, array $params = array())
    {
        try {
            $this->getPdo()->beginTransaction();
            $sth = $this->getPdo()->prepare($sql);
            $this->_execStatement($sth, $params);
            $id = $this->getPdo()->lastInsertId($this->getTableName());
            $this->getPdo()->commit();
            return $id;
        }
        catch (\PDOException $e)
        {
            $this->getPdo()->rollBack();
            $this->handlePDOException($e);
            return false;
        }
    }

    protected function handlePDOException(\PDOException $e)
    {
        $app = App::getInstance();
        $logger = $app['logger.errors'];
        $debug = $app['config']['app.debug'];
        if ($debug) {
            $this->addError('PDO Exception', $e->getMessage());
        }
        $logger->error($e);
    }

    protected function _yield(\PDOStatement $sth)
    {
        try {
            while ($row = $sth->fetch()) {
                $o = $this->newInstance($row);
                $o->postFetch($row, $sth);
                $o->setIsNew(false);
                yield $o;
            }
        }
        finally {
            if ($sth) {
                $sth->closeCursor();
            }
        }
    }

    protected function postFetch($row, $sth)
    {
        foreach($this->post_fetch as $method_name) {
            $this->$method_name($row, $sth);
        }
    }

    public function sanitizeAttributes(array $attributes)
    {
        if ($this->sanitizer()) {
            return $this->doSanitize($attributes, $this->sanitizer());
        }
        if ($this->sanitize) {
            return $this->doSanitize($attributes, $this->sanitize);
        }
        return $attributes;
    }

    protected function doSanitize(array $attributes, $sanitize)
    {
        $attrs = array_diff_key($attributes, $sanitize);
        $sanitized = filter_var_array($attributes, $sanitize, false);
        $attrs = array_merge($attrs, $sanitized);
        return $attrs;
    }

    public function sanitizer()
    {
        return null;
    }

    public function insertIntoQuery($original, $addsql, $key, $before = true)
    {
        $new = '';
        $pos = stripos($original, $key);
        if ($pos !== false) {
            if ($before) {
                $new = substr($original, 0, $pos - 1) . $addsql . substr($original, $pos);
            }
            else {
                $new = substr($original, 0, $pos + strlen($key)) . $addsql . substr($original, $pos + strlen($key));
            }
        }
        return $new;
    }

    public function __get($key)
    {
        return $this->getByFieldname($key);
    }

    public function getByFieldname($fieldname)
    {
        return array_get($this->attributes, $fieldname, null);
    }

    public function __set($key, $value)
    {
        array_set($this->attributes, $key, $value);
    }

    public function __call($func, $args)
    {
        return call_user_func_array(array($this->getPdo(), $func), $args);
    }
}
