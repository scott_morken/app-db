<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/25/15
 * Time: 7:12 AM
 */

namespace Smorken\Db\Models;


use Smorken\Application\App;

abstract class MssqlBase extends Base {

    protected $id_l_quote = '[';

    protected $id_r_quote = ']';


    public function paged($sql, array $params = array(), $page = 0, $countper = 20)
    {
        $page = (int)$page;
        $countper = (int)$countper;
        $orderby = $this->extractOrderBy($sql);
        $wrapped = sprintf('%s OFFSET :offsetXY ROWS FETCH NEXT :limitXY ROWS ONLY', $sql);
        $results = array();
        $sth = $this->getPdo()->prepare($wrapped);
        $sth->bindValue(':offsetXY', $page * $countper, \PDO::PARAM_INT);
        $sth->bindValue(':limitXY', $countper, \PDO::PARAM_INT);
        $this->_execStatement($sth, $params);
        foreach($this->_yield($sth) as $o) {
            $results[] = $o;
        }
        return $results;
    }

    public function extractOrderBy($sql)
    {
        $order = null;
        $ob = 'ORDER BY ';
        $pos = stripos($sql, $ob);
        $found = false;
        if ($pos !== false) {
            $limited = substr($sql, $pos + strlen($ob));
            $i = 0;
            while (($endpos = strpos($limited, ' ', $i)) !== false) {
                if ($endpos > 0 && $limited[$endpos - 1] !== ',') {
                    $order = $ob . substr($limited, 0, $endpos);
                    $found = true;
                    break;
                }
                $i = $endpos + 1;
            }
            if ($endpos === false && $found === false) {
                $order = $ob . $limited;
            }
        }
        return $order;
    }

    public function insert(array $attributes)
    {
        $attrs = $this->getFillableAttributes($attributes);
        $sql = sprintf('INSERT INTO %s (%s)
            OUTPUT INSERTED.%s
            VALUES(%s)',
            $this->getTableName(),
            implode(', ', array_map(array($this, 'quoteIdentifier'), array_keys($attrs))),
            $this->id_column,
            implode(', ', array_fill(0, count($attrs), '?'))
        );
        return $this->_execWithLastId($sql, $this->parameterize($attrs, false));
    }

    protected function _execWithLastId($sql, array $params = array())
    {
        try {
            $this->getPdo()->beginTransaction();
            $sth = $this->getPdo()->prepare($sql);
            $this->_execStatement($sth, $params);
            $id = $sth->fetchColumn();
            if (!$id) {
                $err = $sth->errorInfo();
                if ($err) {
                    $str = $err[2];
                    ob_start();
                    debug_print_backtrace();
                    $str .= "\n" . ob_get_clean();
                    throw new \PDOException($str);
                }
            }
            else {
                $this->getPdo()->commit();
                return $id;
            }
        }
        catch (\PDOException $e)
        {
            $this->getPdo()->rollBack();
            $this->handlePDOException($e);
            return false;
        }
    }
}
