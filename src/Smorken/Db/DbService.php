<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/24/15
 * Time: 10:21 AM
 */

namespace Smorken\Db;

use Particle\Validator\Validator;
use Smorken\Service\Service;

class DbService extends Service {

    public function start()
    {
        $this->name = 'Smorken\Db\Contracts\ConnectionHandler';
    }

    public function load()
    {
        $app = $this->app;
        $this->app->instance($this->getName(), function($c) use ($app) {
            $dbs = $app['config']->get('db', array());
            return new PDOConnectionHandler($dbs);
        });
        $this->app['validator'] = function($c) {
            return new Validator();
        };
    }
}