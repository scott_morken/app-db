<?php
require __DIR__ . '/../../vendor/autoload.php';

class MssqlBaseTest extends PHPUnit_Framework_TestCase {

    public function testExtractOrderBySimple()
    {
        $sql = 'SELECT * FROM something ORDER BY foo';
        $sut = new MsTestStub();
        $extr = $sut->extractOrderBy($sql);
        $this->assertEquals('ORDER BY foo', $extr);
    }

    public function testExtractOrderByMultipleSimple()
    {
        $sql = 'SELECT * FROM something ORDER BY foo, bar';
        $sut = new MsTestStub();
        $extr = $sut->extractOrderBy($sql);
        $this->assertEquals('ORDER BY foo, bar', $extr);
    }

    public function testExtractOrderByMid()
    {
        $sql = 'SELECT * FROM something ORDER BY foo GROUP BY bar';
        $sut = new MsTestStub();
        $extr = $sut->extractOrderBy($sql);
        $this->assertEquals('ORDER BY foo', $extr);
    }

    public function testExtractOrderByMultipleMid()
    {
        $sql = 'SELECT * FROM something ORDER BY foo, bar GROUP BY bar';
        $sut = new MsTestStub();
        $extr = $sut->extractOrderBy($sql);
        $this->assertEquals('ORDER BY foo, bar', $extr);
    }

    public function testInsertIntoQuerySimpleBefore()
    {
        $sql = 'SELECT * FROM something ORDER BY foo, bar';
        $sut = new MsTestStub();
        $extr = $sut->insertIntoQuery($sql, ', fizz ', 'from');
        $this->assertEquals('SELECT *, fizz FROM something ORDER BY foo, bar', $extr);
    }

    public function testInsertIntoQuerySimpleAfter()
    {
        $sql = 'SELECT * FROM something ORDER BY foo, bar';
        $sut = new MsTestStub();
        $extr = $sut->insertIntoQuery($sql, ' fizz', 'from', false);
        $this->assertEquals('SELECT * FROM fizz something ORDER BY foo, bar', $extr);
    }
}

class MsTestStub extends \Smorken\Db\Models\MssqlBase {

}