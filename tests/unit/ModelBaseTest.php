<?php
require __DIR__ . '/../../vendor/autoload.php';

class ModelBaseTest extends PHPUnit_Framework_TestCase
{
    public function testValidatorTrue()
    {
        $sut = new TestStub();
        $r = $sut->validate(['foo' => 'bar']);
        $this->assertTrue($r);
    }

    public function testValidatorFalse()
    {
        $sut = new TestStub();
        $r = $sut->validate(['foo' => 'b4r']);
        $this->assertFalse($r);
    }

    public function testValidatorFalseAppliesMessages()
    {
        $sut = new TestStub();
        $r = $sut->validate(['foo' => 'b4r']);
        $msgs = $sut->getErrors();
        $this->assertArrayHasKey('Alpha::NOT_ALPHA', $msgs['foo']);
    }

}

class TestStub extends \Smorken\Db\Models\Base {

    public function validators()
    {
        $v = $this->getValidator();
        $v->required('foo')->length(3)->alpha();
        return $v;
    }
}